'use strict';
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

//var browser = require("browser-sync");
gulp.task('sass', function() {
    return gulp.src('public/assets/sass/App.scss')
        .pipe($.sass({
            outputStyle: 'compressed' // if css compressed **file size**
        })
        .on('error', $.sass.logError))
        .pipe(gulp.dest('public/assets/css/'));
});

gulp.task('default', ['sass'], function() {
    gulp.watch(['public/assets/sass/**/*.scss'], ['sass']);
});