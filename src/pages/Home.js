import React from 'react'
import { Link } from 'react-router-dom'

export default class Home extends React.Component{
	render(){
		const videos = ["lobo","sand"]
		const video = videos[Math.floor(Math.random()*videos.length)]
		const video_url = "/assets/video/" + video + ".mp4"
		return(
			<div className="fullpage home">
				<video id="fullscreen_video" src={video_url} autoPlay muted loop/>
		    	<div className="vert_centered_text">
		    		<Link to="/vehicle">Tap to Get Started</Link>
		    	</div>
		 	</div>
		)
	}
}
