import React from 'react'
import PageWrap from '../components/PageWrap';
import VehicleCard from '../components/VehicleCard';

const Vehicles = [{
	'type': 'UTV'
},{
	'type': 'ATV'
},{
	'type': 'MOTO'
},{
	'type': 'BIKE'
}]

export default class VehiclesPage extends React.Component{
	render(){
		return(
			<PageWrap page_title="What terrain Do you Ride on?">
    			<div className="vehicle_cards">
					{Vehicles.map( (vehicle, i) => {
						return <VehicleCard key={vehicle.type} type={vehicle.type}/>
					})}
    			</div>
			</PageWrap>
		)
	}
}