import React from 'react'
import PageWrap from '../components/PageWrap';
import Input from '../components/Input';
import TiresAPI from '../api/tires'

const PageStyles = {}

export default class Form extends React.Component{

	constructor(props) {
		super(props);
		this.state = {
			Email: '',
			OptIn: true,
			Tire: this.props.match.params.tire
		};
		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	getCurrentTire(tire){
		return TiresAPI.getTireBySlug(tire)
	}

	handleInputChange(event) {
	    const target = event.target;
	    const value = target.type === 'checkbox' ? target.checked : target.value;
	    const name = target.name;
	    this.setState({
	      [name]: value
	    });
	  }

	handleSubmit(event) {

		event.preventDefault();

		/*return new Promise((resolve, reject) => {

			let values = this.state
			values.Email = document.querySelector("input[type=email]").value

	   	const xhr = new XMLHttpRequest();
	      xhr.open('POST', '/api/form');
	      xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xhr.onreadystatechange = function() {//Call a function when the state changes.
			    if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
			        // Request finished. Do processing here.
			    }
			}
			xhr.onload = function (data) {
				console.log(data);
			  // Request finished. Do processing here.
			};
		   xhr.send({ form: value });
	    });*/
	}

	render(){
		let { tire } = this.props.match.params;
		//const Tire = this.getCurrentTire(tire);
		//console.log( Tire );
		return(
			<PageWrap page_title="" page_class="form_cta">
				<div className="" style={PageStyles}>
					<div className="grid">
						<div className="grid__item col-1-3">
						</div>
						<div className="grid__item col-1-2">
							<h1>Stay up to date on all the latest from CST</h1>
							<p>Even if you’re not taking home tires today, enter your email address and we’ll send you product updates and information on how to get your hands on some sweet CST Racing swag.</p>
							<form onSubmit={this.handleSubmit} method="POST">
								<Input type="email"/>
								<fieldset>
									<input type="checkbox" id="OptIn" name="OptIn" onChange={this.handleInputChange} checked={this.state.OptIn}/>
									<label htmlFor="OptIn">Even if you're not taking home tires today</label>
								</fieldset>
								<input type="hidden" name="Tire" value={tire}/>
								<button className="btn outline" type="submit">Submit</button>
							</form>
				    	</div>
			    	</div>
			 	</div>
		 	</PageWrap>
		)
	}
}