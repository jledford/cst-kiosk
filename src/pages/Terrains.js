import React from 'react'
import PageWrap from '../components/PageWrap';
import TerrainCard from '../components/TerrainCard';
import TiresAPI from '../api/tires';
import queryString from 'query-string'

export default class TerrainsPage extends React.Component{
	getFilteredTerrains(vehicle){
		return TiresAPI.getTerrainsByVehicle(vehicle);
	}
	render(){
		const query = queryString.parse(location.search);
		const TerrainList = this.getFilteredTerrains(query.vehicle);
		return(
		 	<PageWrap page_title="What terrain Do you Ride on?" page_class="terrains_page">
    			<div className="vert_centered_text">
			 		<div className="grid terrains">
			 		{TerrainList.map( (terrain, i) => {
						return <TerrainCard key={i} type={terrain} {...this.props}/>
					})}
					</div>
    			</div>
			</PageWrap>
		)
	}
}
