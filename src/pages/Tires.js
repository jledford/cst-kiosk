import React from 'react'
import PageWrap from '../components/PageWrap'
import TireCard from '../components/TireCard'
import TiresAPI from '../api/tires'
import queryString from 'query-string'


export default class TiresPage extends React.Component{
	getFilteredTires(query){
		return TiresAPI.getTiresByTerrainAndType(query);
	}
	render(){
		const query = queryString.parse(location.search);
		const pagetitle = query.terrain.toUpperCase() + ' ' + query.vehicle.toUpperCase() + ' Tires';
		const TireList = this.getFilteredTires(query);
		return(
		 	<PageWrap page_title={pagetitle} page_class="tires_page">
    			<div className="vert_centered_text">
			 		<div className="tires grid grid--center">
					{TireList.map( (tire, i) => {
						return <TireCard key={i}  tire={tire}/>
					})}
					</div>
    			</div>
			</PageWrap>
		)
	}
}
