import React from 'react'
import PageWrap from '../components/PageWrap';
//import { Link } from 'react-router-dom'
import TiresAPI from '../api/tires'
import MediaGallery from '../components/MediaGallery'
import TireDescription from '../components/TireDescription'
import TireSpecs from '../components/TireSpecs'

const PageStyles = {}

export default class Tire extends React.Component{
	getCurrentTire(tire){
		return TiresAPI.getTireBySlug(tire)
	}
	render(){
		let { tire } = this.props.match.params;
		const Tire = this.getCurrentTire(tire);
		return(
			<div>
				<PageWrap page_title="" page_class="tire_page">
					<div className="" style={PageStyles}>
						<div className="grid grid--tire">
							<div className="grid__item col-1-2">
					    		<MediaGallery tire={Tire}/>
					    	</div>
							<div className="grid__item col-1-2 tire__content">
					    		<TireDescription tire={Tire}/>
					    		<TireSpecs tire={Tire}/>
					    	</div>
				    	</div>
				 	</div>
			 	</PageWrap>
	    	</div>
		)
	}
}