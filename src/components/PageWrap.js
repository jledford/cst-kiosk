import React from 'react'
import PageHeader from './PageHeader';

const PageStyles = {
	//'backgroundImage': 'url(\'/assets/img/home.jpg\')'
}

export default class PageWrap extends React.Component{
	render(){
		const page_classes = ( this.props.page_class ) ? "fullpage " + this.props.page_class : "fullpage";
		return(
			<div className={page_classes} style={PageStyles}>
				<div className="page_wrap">
					<PageHeader page_title={this.props.page_title}/>
					<div className="page_content">
						{this.props.children}
					</div>
		    	</div>
		 	</div>
		)
	}
}