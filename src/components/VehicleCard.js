import React from 'react'
import { Link } from 'react-router-dom'

export default class VehicleCard extends React.Component{
	render(){
		const slug = this.props.type.toLowerCase();
		const title = this.props.type.toUpperCase();
		const className = "vehicle_card "+slug
		const url = "/terrain?vehicle="+slug
		return(
				<div className={className}>
					<Link to={url}></Link>
		    		<div className="vehicle_card__title"><strong>{title}</strong></div>
				</div>
		)
	}
}