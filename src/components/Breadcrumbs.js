import React from 'react'
import { NavLink } from 'react-router-dom'
import queryString from 'query-string'

class Breadcrumbs extends React.Component{

	render(){
		if( location.pathname.indexOf('/tire/') === 0 || location.pathname.indexOf('/form') === 0 ){ return (<div />); }
		const query = queryString.parse(location.search);
		let vehicleLink = '/vehicle';
		let vehicleClass = 'disabled';
		let terrainLink = '/terrain';
		let terrainClass = 'disabled';
		let tireLink = '/tire';
		let tireClass = 'disabled';

		if( location.pathname.match("vehicle") ){
			vehicleClass = "active"
		}else if( location.pathname.match("terrain") ){
			vehicleClass = ""
			terrainClass = "active"
		}else if( location.pathname.match("tire") ){
			vehicleClass = ""
			terrainClass = ""
			tireClass = "active"
		}

		if( query.vehicle ){
			terrainLink = terrainLink + "?vehicle=" + query.vehicle
		}

		if( query.vehicle && query.terrain ){
			tireLink = tireLink + "?vehicle=" + query.vehicle + "&terrain=" + query.terrain
		}

		return(
			<nav className="breadcrumbs">
		      <ul>
		        <li><NavLink to={vehicleLink} className={vehicleClass}>Vehicle</NavLink></li>
		        <li><NavLink to={terrainLink} className={terrainClass}>Terrain</NavLink></li>
		        <li><NavLink to={tireLink} className={tireClass}>Tires</NavLink></li>
		      </ul>
		    </nav>
		)
	}
}

export default Breadcrumbs