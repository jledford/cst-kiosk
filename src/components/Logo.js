import React from 'react'
import { NavLink } from 'react-router-dom'

export default class Logo extends React.Component{

	render(){
		if( location.pathname === "/" ){ return (<div />); }

		return(
			<div className="logo">
			    <NavLink to='/' className='logo-image'>
				<img src="/assets/img/logo.png" alt="CST Tires"/>
				</NavLink>
		    </div>
		)
	}
}
