import React from 'react';
import KeyboardedInput from 'react-touch-screen-keyboard';

export default class Input extends React.Component {
  render() {
    return (
      <KeyboardedInput
        enabled
        type={this.props.type}
        onChange={this.props.onChange}
        value={this.props.value}
        min={this.props.min}
        max={this.props.max}
        step={this.props.step}
        name={this.props.name}
        className={this.props.className}
        placeholder={this.props.placeholder}
        defaultKeyboard="de"
        secondaryKeyboard="us" // optional
      />
    );
  }
}