import React from 'react'

class Back extends React.Component{
	goBack(e){
		e.preventDefault()
		history.back()
	}
	render(){
		//console.log()
		//console.log(location.pathname.indexOf('/form')  !== 0);
		if( location.pathname.indexOf('/tire/') !== 0 && location.pathname.indexOf('/form') !== 0 ){ return (<div />); }
		return(
			<nav className="backbutton">
				<a href="#" onClick={this.goBack} className='back'>Back</a>
		    </nav>
		)
	}
}

export default Back