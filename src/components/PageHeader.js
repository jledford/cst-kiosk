import React from 'react'

export default class PageHeader extends React.Component{
	render(){
		return(
			<div className="page_header">
    			<h1>{this.props.page_title}</h1>
    		</div>
		)
	}
}