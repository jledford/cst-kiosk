import React from 'react'

import Back from './Back';
import Breadcrumbs from './Breadcrumbs';
import Logo from './Logo';

export default class Navigation extends React.Component{
	render(){
		if( location.pathname === "/" ){ return (<div />); }
		return(
			<div>
				<div className="NavPanel">
					<div className="NavPanel__Content"></div>
				</div>
				<Back />
				<Breadcrumbs />
				<Logo />
			</div>
		)
	}
}