import React from 'react'
import renderHTML from 'react-render-html'
//import { Link } from 'react-router-dom'

export default class TireDescription extends React.Component{
	render(){
		const { tire } = this.props;
		return(
			<div className="tire_content">
				<h1>{tire.name}</h1>
				<p>{renderHTML(tire.content)}</p>
			</div>
		)
	}
}
