import React from 'react'
import { Switch, Route } from 'react-router-dom'

import Navigation from '../components/Navigation';
import Home from '../pages/Home';
import Vehicles from '../pages/Vehicles';
import Terrains from '../pages/Terrains';
import Tires from '../pages/Tires';
import Tire from '../pages/Tire';
import Form from '../pages/Form';

class Main extends React.Component{
	render(){
		return(
			<div>
				<Navigation />
			  	<Switch>
				      <Route exact path='/' component={Home}/>
				      <Route path='/vehicle' render={(props) => (
						  <Vehicles {...props} />
					  )}/>
				      <Route path='/terrain' render={(props) => (
						  <Terrains {...props} />
					  )}/>
				      <Route path='/tires' render={(props) => (
						  <Tires {...props}/>
					  )}/>
				      <Route path='/tire/:tire' render={(props) => (
						  <Tire {...props}/>
					  )}/>
				      <Route path='/form/:tire' render={(props) => (
						  <Form {...props}/>
					  )}/>
				</Switch>
			</div>
		)
	}
}

export default Main