import React from 'react'
import { Link } from 'react-router-dom'

export default class TireCard extends React.Component{
	render(){
		const { tire } = this.props;
		let url = "/tire/"+tire.slug;
		let imagesrc = tire.media[0].url;

		return(
			<div className="tire_single grid__item col-1-4">
			<Link className="tire_single-link" to={url}>
				<img src={imagesrc} alt="{tire.name}"/>
				<br />
				<h2 className="tire_single-title">{tire.name}</h2>
			</Link>
			</div>
		)
	}
}
