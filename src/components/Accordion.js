import React from 'react'
import renderHTML from 'react-render-html'

export default class Accordion extends React.Component{
	 constructor(props) {
    	super(props);
    	this.state = {isToggleOn: false};
    	this.handleClick = this.handleClick.bind(this);
  	}

	handleClick(){
		this.setState(prevState => ({
	      isToggleOn: !prevState.isToggleOn
	    }));
	}
	render(){
		const { size, specs } = this.props;
		let id = "accordion_"+size
		return(
			<div id={id} className={this.state.isToggleOn ? 'accordion open' : 'accordion'}>
				<div className="accordion_title" onClick={this.handleClick}>
					{ size }
				</div>
				{ renderHTML(specs) }
			</div>
		)
	}
}