import React from 'react'
import { Link } from 'react-router-dom'

export default class TerrainCard extends React.Component{
	render(){
		const slug = this.props.type.toLowerCase().replace(" ","-");
		const title = this.props.type.toUpperCase();
		const { search } = this.props.location
		const url = "/tires" + search + "&terrain="+slug
		return(
			<Link className="terrain grid__item col-1-2" to={url}>{title}</Link>
		)
	}
}
