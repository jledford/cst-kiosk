import React from 'react'
import ImageGallery from 'react-image-gallery';

export default class MediaGallery extends React.Component{
	render() {
		const { media } = this.props.tire;
		const images = [];

		media.forEach(function(val){
			images.push({
				original: val.url,
				thumbnail: val.url
			})
		});

		return (
		  <ImageGallery
		    items={images}
		    slideInterval={2000}
		    showNav={false}
		    showFullscreenButton={false}
		    showPlayButton={false}/>
		);
	}
}
