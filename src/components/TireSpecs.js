import React from 'react'
//import renderHTML from 'react-render-html'
import Accordion from './Accordion'
//import { Link } from 'react-router-dom'

export default class TireSpecs extends React.Component{
	render(){
		const { tire } = this.props;
		if(tire.sizes == null){ return( <div /> ) }
		return(
			<div className="specs">
				{
					tire.sizes.map((row, index) => {
						return <Accordion key={index} size={row.size} specs={row.specs}/>
					})
				}
			</div>
		)
	}
}
