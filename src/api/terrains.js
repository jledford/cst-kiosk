import terrains from '../../public/assets/data/terrains.json'
const TerrainAPI = {
	'terrains': terrains,
	all: function() { return this.terrains},
	getTireByName: function(name) {
		const isTire = p => p.name === name
		return this.terrains.find(isTire)
	},
	getTireBySlug: function(slug) {
		const isTire = p => p.slug === slug
		return this.terrains.find(isTire)
	},
	getTiresByVehicle: function(vehicle) {
		const isTire = p => p.vehicle.indexOf(vehicle) !== -1
		return this.terrains.filter(isTire)
	},
	getTiresByTerrain: function(terrains) {
		const isTire = p => p.terrains.indexOf(terrains) !== -1
		return this.terrains.filter(isTire)
	},
	getTiresByTerrainAndType: function(query) {
		let return_array = {};
		if(query.vehicle){
			return_array = this.getTiresByVehicle( query.vehicle.toLowerCase() );
		}
		if( query.terrain ){

		}
		const isTire = p => p.terrains.indexOf( query.terrain ) !== -1
		return return_array.filter(isTire)
	}
}

export default TerrainAPI