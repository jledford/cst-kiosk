import tires from '../../public/assets/data/tires.json'
const TiresAPI = {
	'tires': tires,
	all: function() { return this.tires },
	getTireByName: function(name) {
		const isTire = p => p.name === name
		return this.tires.find(isTire)
	},
	getTireBySlug: function(slug) {
		const isTire = p => p.slug === slug
		return this.tires.find(isTire)
	},
	getTiresByVehicle: function(vehicle) {
		if( vehicle === 'utv' ){ vehicle = 'atv' }
		const isTire = p => p.vehicle.indexOf(vehicle) !== -1
		return this.tires.filter(isTire)
	},
	getTiresByTerrain: function(terrains) {
		const isTire = p => p.terrains.indexOf(terrains) !== -1
		return this.tires.filter(isTire)
	},
	getTerrainsByVehicle: function(vehicle){
		const vehicle_terrains = this.getTiresByVehicle(vehicle).map(p => p.terrains)
		const filteredTerrains = []
		vehicle_terrains.forEach(function(item){
			item.forEach(function(val){
				if(filteredTerrains.indexOf(val) === -1) {
					filteredTerrains.push(val)
				}
			})
		})
		return filteredTerrains
	},
	getTiresByTerrainAndType: function(query) {
		let return_array = {};
		if(query.vehicle){
			return_array = this.getTiresByVehicle( query.vehicle.toLowerCase() );
		}
		if( query.terrain ){

		}
		const isTire = p => p.terrains.indexOf( query.terrain ) !== -1
		return return_array.filter(isTire)
	},
	getTireMedia: function(terrains) {
		const files = this.tires.map(p => p.media)
		const media = []
		files.forEach(function(item){
			item.forEach(function(val){
					media.push(val.url)
			})
		})
		return media
	}
}

export default TiresAPI